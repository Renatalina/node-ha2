const express = require("express");
const MongoClient = require("mongodb").MongoClient;
const ObjectId = require("mongodb").ObjectId;
const jwt = require("jsonwebtoken");

const app = express();
const jsonParser = express.json();

const uri =
  "mongodb+srv://renatalina:htv,02602@cluster0.jrpld.mongodb.net/test";

const mongoClient = new MongoClient(uri);

const tokenKey = "1a2b-3c4d-5e6f-7g8h";
let dbClient;
const meUser = {};

app.use(express.static(__dirname + "/public"));

app.use((req, res, next) => {
  if (req.headers.authorization) {
    jwt.verify(
      req.headers.authorization.split(" ")[1],
      tokenKey,
      (err, payload) => {
        if (err) next();
        else if (payload) {
          for (const user of users) {
            if (user.password === payload.password) {
              req.user = user;
              next();
            }
          }

          if (!req.user) next();
        }
      }
    );
  }

  next();
});

mongoClient.connect(function (err, client) {
  if (err) return console.log(err);
  dbClient = client;
  app.locals.collection = client.db("chat_db").collection("users");
  app.locals.notes = client.db("chat_db").collection("notes");
  app.listen(8080, function () {
    console.log("Сервер ожидает подключения на 8080");
  });
});

app.post("/api/auth/login", jsonParser, function (req, res) {
  try {
    if (!req.body) return res.status(400).send({ message: "string" });

    const userName = req.body.username;
    const userPassword = req.body.password;

    const user = { username: userName, password: userPassword };

    const collection = req.app.locals.collection;
    collection.findOne(user, function (err, result) {
      if (err) return res.status(400).send({ message: "string" });

      if (result) {
        meUser._id = result._id.toHexString();
        meUser.name = userName;
        meUser.password = userPassword;

        res.status(200).send({
          message: "Success",
          jwt_token: `JWT ${jwt.sign({ password: userPassword }, tokenKey)}`,
        });
      }
    });
  } catch (error) {
    res.status(500).send({ message: "string" });
  }
});

app.post("/api/auth/register", jsonParser, function (req, res) {
  try {
    const userName = req.body.username;
    const userPassword = req.body.password;

    const user = { name: userName, password: userPassword };

    const collection = req.app.locals.collection;
    collection.insertOne(user, function (err, result) {
      if (err) return res.status(400).send({ message: "string" });

      meUser._id = result.insertedId.toHexString();
      meUser.name = userName;
      meUser.password = userPassword;
      jwt_token = "0";
      res.status(200).send({ message: "Success" });
    });
  } catch (error) {
    res.status(500).send({ message: "string" });
  }
});

app.get("/api/users/me", function (req, res) {
  try {
    const collection = req.app.locals.collection;

    collection.findOne({ _id: new ObjectId(meUser._id) }, function (err, user) {
      if (err) {
        res.status(400).send({ message: "string" });
        return console.log(err);
      }
      res.status(200).send({
        user: {
          _id: meUser._id,
          username: meUser.name,
          createDate: new Date().toLocaleDateString(),
        },
      });
    });
  } catch (error) {
    res.status(500).send({ message: "sting" });
  }
});

app.delete("/api/users/me", function (req, res) {
  try {
    const collection = req.app.locals.collection;
    collection.findOneAndDelete(
      { _id: new ObjectId(meUser._id) },
      function (err, result) {
        if (err) {
          res.status(400).send({ message: "string" });
          return console.log(err);
        }
        res.status(200).send({ message: "Success" });
      }
    );
  } catch (error) {
    res.status(500).send({ message: "string" });
  }
});

app.patch("/api/users/me", jsonParser, function (req, res) {
  try {
    const id = new ObjectId(meUser._id);
    const userPassword = req.body.oldPassword;
    const newPassword = req.body.newPassword;

    const collection = req.app.locals.collection;

    collection.findOneAndUpdate(
      { _id: id },
      { $set: { password: newPassword } },
      { returnDocument: "after" },
      function (err, result) {
        if (err) return res.status(400).send({ message: "string" });

        res.status(200).send({ message: "Success" });
      }
    );
  } catch (error) {
    res.status(500).send({ message: "string" });
  }
});

app.get("/api/notes", function (req, res) {
  try {
    const collection = req.app.locals.notes;
    const verifyPassword = req.headers.authorization;
    if (!verifyPassword)
      return res.status(403).send({ error: "You are not varifying!" });
    else {
      jwt.verify(verifyPassword.split("")[1], tokenKey, (err, user) => {
        if (err) return res.status(500).send("server error");
      });
    }

    collection.find({ userId: meUser._id }).toArray(function (err, notes) {
      if (err) return res.status(400).send({ message: "string" });
      res.status(200).send({
        offset: 0,
        limit: 0,
        count: 0,
        notes: [notes],
      });
    });
  } catch (error) {
    res.status(500).send({ message: "string" });
  }
});

app.post("/api/notes", jsonParser, function (req, res) {
  try {
    if (!req.body) return res.status(400).send({ message: "string" });
    const verifyPassword = req.headers.authorization;
    if (!verifyPassword)
      return res.status(403).send({ error: "You are not varifying!" });
    else {
      jwt.verify(verifyPassword.split("")[1], tokenKey, (err, user) => {
        if (err) return res.status(500).send("server error");
      });
    }
    const text = req.body.text;
    const date = new Date().toDateString();
    const note = {
      userId: meUser._id,
      completed: false,
      text: text,
      createDate: date,
    };

    const collection = req.app.locals.notes;
    collection.insertOne(note, function (err, result) {
      if (err) return res.status(400).send({ message: "string" });
      res.status(200).send({ message: "Success" });
    });
  } catch (error) {
    res.status(500).send({ message: "string" });
  }
});

app.get("/api/notes/:id", function (req, res) {
  try {
    const id = new ObjectId(req.params.id);
    const collection = req.app.locals.notes;
    collection.findOne({ _id: id }, function (err, note) {
      if (err) return res.status(400).send({ message: "string" });

      res.status(200).send({
        note: {
          _id: note._id.toHexString(),
          userId: meUser._id,
          completed: false,
          text: note.text,
          createdDate: note.createDate,
        },
      });
    });
  } catch (error) {
    res.status(500).send({ message: "string" });
  }
});

app.put("/api/notes/:id", jsonParser, function (req, res) {
  const verifyPassword = req.headers.authorization;
  if (!verifyPassword)
    return res.status(403).send({ error: "You are not varifying!" });
  else {
    jwt.verify(verifyPassword.split("")[1], tokenKey, (err, user) => {
      if (err) return res.status(500).send("server error");
    });
  }

  const id = new ObjectId(req.params.id);
  const userId = meUser._id;
  const text = req.body.text;

  const collection = req.app.locals.notes;
  collection.findOneAndUpdate(
    { _id: id },
    {
      $set: {
        text: text,
      },
    },
    { returnDocument: "after" },
    function (err, result) {
      if (err) return res.status(500).send({ message: "string" });

      res.status(200).send({ message: "Success" });
    }
  );
});

app.patch("/api/notes/:id", jsonParser, function (req, res) {
  try {
    const id = new ObjectId(req.params.id);
    if (!id) return res.status(400).send({ message: "string" });

    const collection = req.app.locals.notes;
    collection.findOneAndUpdate(
      { _id: id },
      { $set: { completed: true } },
      { returnDocument: "after" },
      function (err, result) {
        if (err) return res.status(400).send({ message: "string" });

        res.status(200).send({ message: "Success" });
      }
    );
  } catch (error) {
    res.status(500).send({ message: "string" });
  }
});

app.delete("/api/notes/:id", function (req, res) {
  try {
    const id = new ObjectId(req.params.id);
    const collection = req.app.locals.notes;
    collection.findOneAndDelete({ _id: id }, function (err, result) {
      if (err) {
        return res.status(400).send({ message: "string" });
      }
      res.status(200).send({ message: "Success" });
    });
  } catch (error) {
    res.status(500).send({ message: "string" });
  }
});

process.on("SIGINT", () => {
  dbClient.close();
  process.exit();
});
